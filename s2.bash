#!/bin/bash

read -p "Enter your last name: " lname
read -p "Enter your first name: " fname
echo "Enter your birthdate with the following format (YYYY MM DD): "
read year month
echo
yearnow=$(date '+%Y')

age=$(($yearnow-$year))

echo "Hello, $fname $lname! You're $age years old!"